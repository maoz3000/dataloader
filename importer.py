from postgresql_client import PostgresqlClient
import csv
import yaml
import logging
import os.path


def simple_value_quote(v: str) -> str:
    return f"'{v}'"

def array_value_quote(vals: str) -> str:
    #wrap each element with double quotes
    vals = map(lambda v: v.strip(), vals[1:-1].split(','))
    vals = map(lambda v: v.replace("'", '"'), vals)

    # join values with comma
    vals = ','.join(vals)

    # wrap all list with curly braces (i.e '{e1, e2, ..., en}')
    # This was got a bit ugly becuase of the escaping of the curly braces...
    return f"'{{{vals}}}'"

def is_array(v:str) -> bool:
    return v.startswith('[') and v.endswith(']')

#I assume Postgress table alreadt exist with the following conditions:
#    - same column names
#    - extra cloumns: fetched, fetch, ingest_time

class Importer:
    def __init__(self, config_filepath:str='config.yaml') -> None:
        with open(config_filepath, 'r') as config_file:
            self._config = yaml.load(config_file)
            #self._connection = None
            self._db_client = PostgresqlClient(self._config.get('source').get('db').get('connection_string'))
            self._columns = []
            self._bulk_size = 20

    def __enter__(self):
        self._db_client.connect()
        return self
    
    def __exit__(self, exc_type,exc_value, exc_traceback):
        self._db_client.close()

    def load(self, filepath:str, table:str=''):
        self._columns = []
        if not table:
            table, _ = os.path.splitext(os.path.basename(filepath))
        with open(filepath, 'r') as f:
            reader = csv.reader(f)
            
            #Read column names
            for row in reader:
                if not self.columns:
                    self._columns = list(map(lambda column: column.replace(' ', '_'), row))
                    break            
            #Read data rows and insert as bulks.
            for bulk in self.chunks(list(reader), self.bulk_size):     
                bulk_values = []
                for row in bulk:                    
                    quote_func = lambda v: array_value_quote(v) if is_array(v) else simple_value_quote(v)
                    quoted_row_values = map(quote_func, row)
                    bulk_values.append(f"({','.join(list(quoted_row_values))})")
                sql_cmd = f"INSERT INTO {table} ({','.join(self.columns)}) VALUES {','.join(bulk_values)}"
                logging.debug(f"Inseting bulk into table {table} using the following sql statement: {sql_cmd}")
                self._db_client.execute(sql_cmd)
                logging.info(f"Inserted {self.bulk_size} rows into {table} table")
            self._db_client.commit()

    @staticmethod
    def chunks(lst, n):
        """Yield successive n-sized chunks from lst."""
        for i in range(0, len(lst), n):
            yield lst[i:i + n]

    @property
    def columns(self) -> list:
        return self._columns
    
    @property
    def bulk_size(self) -> int:
        return self._bulk_size

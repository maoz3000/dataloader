from sha256_transformation import Sha256Transformation
from importer import Importer
from data_loader import DataLoader
import logging
import argparse

logging.basicConfig(level='INFO', format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

my_parser = argparse.ArgumentParser(description='Viz data transformer',
                                    epilog='Fill free to ask questions - maoz3000@gmail.com')

my_parser.add_argument('-m',
                       '--execmode',
                       action='store',
                       default='loader',
                       help='execute as data importer or data loader')

# Execute parse_args()
args = my_parser.parse_args()

if args.execmode == 'importer':
    source_files = ['source_files/users_de.csv', 'source_files/notifications_de.csv', 'source_files/scans_and_viz_results_de.csv']
    with Importer() as importer:    
        for source_csv in source_files:
            logging.info(f"Importing csv file {source_csv}...")
            importer.load(source_csv)

elif args.execmode == 'loader':
    hash_transformer = Sha256Transformation()
    with DataLoader() as dl:
        while(True):        
            dl.copy('users_de', primary_key='user_uid', 
                    transformations_map={
                        'name': hash_transformer,
                        'address': hash_transformer,
                        'user_uid': hash_transformer
             })
            dl.copy('notifications_de', primary_key='study_uid', 
                 transformations_map={
                     'study_uid': hash_transformer,
                     'patient': hash_transformer,
                     'users': hash_transformer
                })
            dl.copy('scans_and_viz_results_de', primary_key='study_uid', 
                    transformations_map={
                        'study_uid': hash_transformer,
                        'first_acquired': hash_transformer,
                        'patient_first_acquired': hash_transformer,
                        'patient': hash_transformer
                    })        
else:
    print('unsupprted values for execution mode')
    exit(1)
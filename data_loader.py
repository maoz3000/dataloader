import yaml
import logging
import pandas.io.sql as psql
import io
from psycopg2 import DatabaseError
from postgresql_client import PostgresqlClient


class DataLoader:
    def __init__(self, config_filepath: str='config.yaml') -> None:
        with open(config_filepath, 'r') as config_file:
            self._config = yaml.load(config_file)
        self._source_db_client = PostgresqlClient(self._config.get('source').get('db').get('connection_string'))
        self._target_db_client = PostgresqlClient(self._config.get('target').get('db').get('connection_string'))
        self._bulk_size = self._config.get('bulk_size', 10)

    def __enter__(self):
        self._source_db_client.connect()
        self._target_db_client.connect()
        return self
    
    def __exit__(self, exc_type,exc_value, exc_traceback):
        self._source_db_client.close()
        self._target_db_client.close()

    def copy(self, source_table: str, primary_key:str, dest_table: str='', transformations_map: dict={}):
        if not source_table:
            raise ValueError("source_table name must be provided")
        if not dest_table:
            logging.info(f"Target able name was not provided, assumming table name is as source ({source_table})")
            dest_table = source_table                
        try:
            bulk = self.read_from_source(source_table)
            original_ids = list(bulk[primary_key])
            if not original_ids:
                logging.info(f'No bulk was fetched, guesss no new records int source tabel [{source_table}]')
                return
            bulk = self.execute_transformations(dest_table, bulk, transformations_map)
            self.write_to_dest(dest_table, bulk)
            self.commit_bulk(bulk, source_table, primary_key, original_ids)
        except (Exception, DatabaseError) as error:
            logging.exception(error, f"Rolling back current bulk [table {source_table}]")
            self._source_db_client.rollback()
            self._target_db_client.rollback()

        
    def read_from_source(self, table: str) -> list:
        fetch_command = f"SELECT * FROM {table} WHERE is_fetched=false ORDER BY creation_time FOR UPDATE LIMIT({self.bulk_size})"
        df = psql.read_sql(fetch_command, self._source_db_client.connection)
        return df

    def execute_transformations(self, table, df, transformations_map):
        for column, transformation in transformations_map.items():
            df[column] = df[column].map(transformation.transform)            
        return df
    
    def write_to_dest(self, dest_table, bulk):
        #tmp_df = 'x'
        tmp_df = io.StringIO()
        bulk.to_csv(tmp_df, header=False, index=False, sep="|")
        tmp_df.seek(0)
        #with open(tmp_df, 'r') as f:
        self._target_db_client.cursor.copy_from(tmp_df, dest_table, sep='|')        
    
    def commit_bulk(self, bulk, table: str, primary_key: str, original_ids: list):        
        original_ids = map(lambda id: f"'{id}'", original_ids)
        set_fetch_flag_cmd = f"UPDATE {table} SET is_fetched=true WHERE {primary_key} IN ({','.join(original_ids)})"
        self._source_db_client.execute(set_fetch_flag_cmd)
        self._target_db_client.commit()
        self._source_db_client.commit()

    @property
    def bulk_size(self):
        return self._bulk_size

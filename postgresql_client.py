import psycopg2
import logging
import urllib.parse as up

class PostgresqlClient:
    def __init__(self, connection_string) -> None:
        self._connection_string = connection_string
        self._connection = None
        self._cursor = None
        #self._init_db_connection()

    def connect(self, verify_connection:bool=False):
        """
        connection and cursor properties will only be set initializing conntection using this function.
        """
        try:
            up.uses_netloc.append("postgres")
            url = up.urlparse(self._connection_string)
            self._connection = psycopg2.connect(database=url.path[1:],
                user=url.username,
                password=url.password,
                host=url.hostname,
                port=url.port
            )
            self._cursor = self._connection.cursor()
            if verify_connection:
                #Print PostgreSQL Connection properties
                print(self._connection.get_dsn_parameters(),"\n")
                #Print PostgreSQL version                
                self._cursor.execute("SELECT version();")
                record = self._cursor.fetchone()
                print("You are connected to - ", record,"\n")
            #return cursor
        except (Exception, psycopg2.Error) as error:
            print("Error while connecting to PostgreSQL", error)
            exit(1)        

    def execute(self, sql_command):
        logging.debug(f"Executing sql command: '{sql_command}'")
        self.cursor.execute(sql_command)
        return self.cursor

    def commit(self):
        self.connection.commit()

    def rollback(self):
        self.connection.rollback()

    def close(self):
        if(self.connection):
            self.connection.close()
            logging.info("PostgreSQL connection is closed")

    @property
    def connection(self):
        return self._connection
    
    @property
    def cursor(self):
        return self._cursor
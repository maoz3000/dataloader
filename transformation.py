import hashlib
import logging

class Transformation:
    def transform(self, source_value):        
        logging.debug(f"Transforming {source_value} using f{self.__class__}")
        if self._is_array(source_value):
            source_value = self._to_list(source_value)
        return source_value
        
    def _is_array(self, value):
        return type(value) == str and value.startswith('{') and value.endswith('}')
        
    def _to_list(self, value) -> list:
        return value.rstrip('}').lstrip('{').split(',')
# Viz Data Loader Coding Assignment #

### What is this repository for? ###
Service that reads data from a source database, runs some transformations, and saves
the data into a target database.
Current Database in Postgres, both the source and the target (running on two different instances).

### How do I get set up? ###
Following commands were used on Ubuntu:

1. postgresql-devel must be installed.
`sudo apt-get install -y libpq-dev`

2. Create Python Virtual environment and install dependecy packages:
`virtualenv -p python3 viz-venv/`

`source viz-venv/bin/activate`

`pip install -r requirements.txt`

3. Creating source tables in DB:
```
REATE TABLE IF NOT EXISTS public.users_de
(
	user_uid VARCHAR(150),
    name VARCHAR(150) NOT NULL,
    address VARCHAR(150),    
    creation_time timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    is_fetched boolean NOT NULL DEFAULT FALSE,
    PRIMARY KEY (user_uid)
)
WITH (
    OIDS = FALSE
);

CREATE TABLE IF NOT EXISTS public.notifications_de
(
	study_uid VARCHAR(150) NOT NULL,
	notification_time timestamp without time zone NOT NULL,
	patient VARCHAR(150) NOT NULL,
	users VARCHAR(150) [],
    creation_time timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    is_fetched boolean NOT NULL DEFAULT FALSE,
    PRIMARY KEY (study_uid)
)
WITH (
    OIDS = FALSE
);

CREATE TABLE IF NOT EXISTS public.scans_and_viz_results_de
(
    study_uid VARCHAR(150) NOT NULL,
    viz_lvo boolean NOT NULL DEFAULT FALSE,
    first_acquired VARCHAR(150) NOT NULL,
    patient_first_acquired VARCHAR(150) NOT NULL,
    patient_institution  VARCHAR(30) NOT NULL,
    patient VARCHAR(150) NOT NULL,   
    creation_time timestamp without time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    is_fetched boolean NOT NULL DEFAULT FALSE,
    PRIMARY KEY (study_uid)
)
WITH (
    OIDS = FALSE
);
```
### Configuration File ###
This code makes use of `yaml` configuration file for configuring databases connection string. Default file path is `./config.yaml`. Yaml was chosen for its simplicity and ability to support comments.

### Running the code ###
Main file is `runner.py` and can be executed in 2 modes:
- importer: import sample datasets from `csv` files into source DB.
- loader: Transfrom data from source tables and ingest into target.
Default execution mode is `loader` and can be changed using `--execmode` parameter.

Example:
importer mode execution:
`python runner.py --execmode importer`

### FAQ ###
`Error: pg_config executable not found.` when installing requirements file
Make sure postgresql-devel are installed.

Assumptions
The code was only tested on Ubuntu 20.04, and uses Python `os.path` module so UNIX-style paths may assumed. Hence running from Windows may end up with an error 
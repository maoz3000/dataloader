import hashlib
from posixpath import join
from transformation import Transformation



class Sha256Transformation(Transformation):
    def transform(self, source_value):
        source_value = super().transform(source_value)
        if type(source_value) == list:
            new_list = list(map(lambda v: hashlib.sha256(v.encode('utf-8')).hexdigest(), source_value))
            new_list = list(map(lambda v: f"'{v}'", new_list))
            new_list = '{' + ', '.join(new_list) + '}'
            return new_list
        return hashlib.sha256(str(source_value).encode('utf-8')).hexdigest()
